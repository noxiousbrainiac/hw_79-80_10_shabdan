const express = require('express');
const cors = require('cors');
const mysqlDb = require('./mysqlDb');
const subjects = require('./routes/subjects');
const categories = require('./routes/categories');
const locations = require('./routes/locations');

const port = 8000;

const app = express();
app.use(express.json());
app.use(cors());

app.use('/subjects', subjects);
app.use('/categories', categories);
app.use('/locations', locations);

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
    console.log('You are on port: ', port);
});