DROP database if exists homework_database;
CREATE database if not exists homework_database;

use homework_database;

create table if not exists categories (
	id int not null auto_increment primary key ,
	title varchar(255) not null,
	description text null
);

create table if not exists locations (
	id int not null auto_increment primary key ,
	title varchar(255) not null,
	description text null
);

create table subjects (
	id int not null auto_increment primary key,
	category_id int not null,
	location_id int not null,
	title varchar(255) not null,
	description text null,
	image varchar(255) null,
	delivery_date datetime default CURRENT_TIMESTAMP not null,
	constraint subjects_categories_id_fk
	foreign key (category_id)
	references categories (id)
	on update cascade
    on delete restrict,
	constraint subjects_locations_id_fk
	foreign key (location_id)
	references locations (id)
	on update cascade
    on delete restrict
);

insert into categories (title, description)
values ('Мебели', 'Шведская мебель европейского качества'),
       ('Компьютерная техника','Ноутбуки, моноблоки, мониторы, периферия'),
       ('Бытовая техника','Холодильники, пылесосы, телефизоры');

insert into locations (title, description)
values ('Бишкек', 'ул.Горького 24'),
       ('Кант', 'ул.Московская 24'),
       ('Ош', 'ул.Советская 24');

insert into subjects (category_id, location_id, title, description, image)
values (1, 3, 'Заказ №1', 'Оплачено онлайн', null),
       (2, 2, 'Заказ №2', 'Покупатель сам заберет товар', null),
       (3, 1, 'Заказ №3', 'Покупка в кредит', null);
