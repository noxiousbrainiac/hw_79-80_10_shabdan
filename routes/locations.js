const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [locations] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['locations']);
        res.send(locations);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [locations] = await mysqlDb.getConnection().query(
            `SELECT * FROM ?? where id = ?`, ['locations', req.params.id])
        if (!locations) {
            return res.status(404).send({error: 'Location not found'});
        }
        res.send(locations[0]);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        if (!req.body.title || !req.body.description) {
            return res.status(400).send({error: 'Data not valid'});
        }

        const location = {
            title: req.body.title,
            description: req.body.description,
        };

        const newLocation = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (title, description) values (?, ?)',
            ['locations', location.title, location.description]
        );

        res.send({
            ...location,
            id: newLocation[0].insertId
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put('/:id', async (req, res) => {
    try {
        const location = {
            title: req.body.title,
            description: req.body.description,
        };

        await mysqlDb.getConnection().query(
            'UPDATE ?? SET ? where id = ?',
            ['locations', {...location}, req.params.id]);

        res.send({message: `Update successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await  mysqlDb.getConnection().query(
            'DELETE FROM ?? WHERE id = ?',
            ['locations', req.params.id]
        );

        res.send({message: `Deleted successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;