const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [categories] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['categories']);
        res.send(categories);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [categories] = await mysqlDb.getConnection().query(
            `SELECT * FROM ?? where id = ?`, ['categories', req.params.id])
        if (!categories) {
            return res.status(404).send({error: 'Category not found'});
        }

        res.send(categories[0]);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', async(req, res) => {
    try {
        if (!req.body.title || !req.body.description) {
            return res.status(400).send({error: 'Data not valid'});
        }

        const category = {
            title: req.body.title,
            description: req.body.description,
        };

        const newCategory = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (title, description) values (?, ?)',
            ['categories', category.title, category.description]
        );

        res.send({
            ...category,
            id: newCategory[0].insertId
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put('/:id', async (req, res) => {
    try {
        const category = {
            title: req.body.title,
            description: req.body.description,
        };

        await mysqlDb.getConnection().query(
            'UPDATE ?? SET ? where id = ?',
            ['categories', {...category}, req.params.id]);

        res.send({message: `Update successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await  mysqlDb.getConnection().query(
            'DELETE FROM ?? WHERE id = ?',
            ['categories', req.params.id]
        );

        res.send({message: `Deleted successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;