const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const mysqlDb = require('../mysqlDb');
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [subjects] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['subjects']);
        res.send(subjects);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [subjects] = await mysqlDb.getConnection().query(
            `SELECT * FROM ?? where id = ?`, ['subjects', req.params.id])
        if (!subjects) {
            return res.status(404).send({error: 'Subject not found'});
        }

        res.send(subjects[0]);
    } catch (e) {
        res.status(500).send(e);
    }

});

router.post('/', upload.single('image'), async(req, res) => {
    try {
        if (!req.body.title || !req.body.description || !req.body.location_id || !req.body.category_id) {
            return res.status(400).send({error: 'Data not valid'});
        }

        const subject = {
            title: req.body.title,
            description: req.body.description,
            category_id: req.body.category_id,
            location_id: req.body.location_id
        };

        if (req.file) {
            subject.image = req.file.filename;
        }

        const newSubject = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (category_id, location_id, title, description, image) values (?, ?, ?, ?, ?)',
            ['subjects', subject.category_id,  subject.location_id, subject.title, subject.description, subject.image]
        );

        res.send({
            ...subject,
            id: newSubject[0].insertId
        });
    } catch (e) {
        res.status(500).send(e);
    }

});

router.put('/:id', upload.single('image'), async (req, res) => {
    try {
        const subject = {
            title: req.body.title,
            description: req.body.description,
            category_id: req.body.category_id,
            location_id: req.body.location_id
        };

        if(req.file) subject.image = req.file.filename;

        await mysqlDb.getConnection().query(
            'UPDATE ?? SET ? where id = ?',
            ['subjects', {...subject}, req.params.id]);

        res.send({message: `Update successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }

});

router.delete('/:id', async (req, res) => {
    try {
        await  mysqlDb.getConnection().query(
            'DELETE FROM ?? where id = ?',
            ['products', req.params.id]
        );

        res.send({message: `Deleted successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;